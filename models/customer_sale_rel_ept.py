from odoo import fields,models,api
from datetime import date

class CustomerSaleRelEpt(models.Model):
    _name = 'customer.sale.rel.ept'
    _description = 'Customer Sale Relation'
    _inherit = ['mail.thread']
    
    date_ept = fields.Date('Date',default = date.today())
    so_id = fields.Many2one('sale.order', 'Sale Order')
    name = fields.Char('Customer Name')
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    invoice_paid_amount = fields.Float('Invoice Paid Amount',compute='_invoice_paid_amount')
    invoice_remaining_amount = fields.Float('Invoice Remaining Amount',compute='_invoice_remaining_amount')
    products = fields.Char('Products')
    
    @api.one
    def _invoice_paid_amount(self):
        if self.so_id:
            res = 0.0
            inv = self.so_id.invoice_ids
            for invoice in inv:
                if invoice.state == 'paid':
                    res += invoice.amount_total
            self.invoice_paid_amount = res
                    
    
    @api.one
    def _invoice_remaining_amount(self):
        if self.so_id:
            res = 0.0
            inv = self.so_id.invoice_ids
            for invoice in inv:
                if invoice.state == 'open':
                    res += invoice.amount_total
            self.invoice_remaining_amount = res
    
    @api.multi
    def set_product(self):
        prods = []
        res = self.so_id.order_line
        for id in res:
            prods.append(id.product_id.name)
        prodString = ','.join(prods)
        self.write({
            'products' : prodString
            })
    
    @api.onchange('so_id')
    def onchange_sale_order(self):
        if self.so_id:
            self.name = self.so_id.partner_id.name
            self.warehouse_id = self.so_id.warehouse_id
            
class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    customer_sale_rel_ids = fields.One2many('customer.sale.rel.ept','so_id','Customer Sale Relation')
    
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        super(SaleOrderLine, self).product_uom_change()
#         if self.product_uom:
#             self.price_unit = (self.env['account.tax']._fix_tax_included_price(self._get_display_price(self.product_id), self.product_id.taxes_id, self.tax_id))/self.product_uom
        if self.product_uom_qty:
            self.price_unit = (self.env['account.tax']._fix_tax_included_price(self._get_display_price(self.product_id), self.product_id.taxes_id, self.tax_id))/self.product_uom_qty
            
    