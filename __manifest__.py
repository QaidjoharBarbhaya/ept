# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Customer Sale Relation',
    'version': '1.0',
    'category': 'Sales',
    'author': "Emipro Technologies (P) Ltd.",
    'summary': 'Sales, Sale Order, CRM',
    'description': """
Customer Sale Relation
==================================

This module allows sales managers to view and analyse the received and pending payments of numerous invoices of a single sale order at one place.

Key Features
------------
    * Computes received and pending payments of numerous invoices of a single Sale Order.
    * Allows sales managers to view the computed data beneath the very selected Sale Order.
    """,
    'website': 'www.emiprotechnologies.com',
    'depends': ['account','stock'],
    'data': [
        'views/menuitem.xml',
        'views/customer_sale_rel_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
